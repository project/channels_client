<?php

/**
 * 
 * Return the list of the supported types
 * @return array
 */
function channels_client_node_get_types() {
  return array_keys(node_get_types());
}

/**
 * 
 * Save a given entity in the local database
 * @param string $type The type of entity (node, user...)
 * @param int $id The ID of the entity
 * @param array $fields The entity's fields
 * 
 * @return bool Return TRUE when the entity is saved, or FALSE when an error occured.
 */
function channels_client_save_entity($type, $id, array $fields) {
  
  global $user;
  
  if( $type == 'node') {
    
    
    //watchdog('debug', 'user data', $user);
  
    $node = node_load($id);
      
    if (!$node){
      $node = new stdClass();
      $is_new = TRUE;
    }
    else $is_new = FALSE;
  
    foreach ($fields as $field_key => $field) {
      if($is_new && $field_key != 'nid') {
        $node->{$field_key} = $field['value'];
      }
    }

    $hook = 'channels_client_alter_node';
    foreach (module_implements($hook) as $module) {
      $function = $module .'_'. $hook;
      $node = call_user_func_array($function, array($node));
    }

    node_save($node);
    
    if(!$node->nid)
      return FALSE;
  }

  return TRUE;
}

/**
 * 
 * Alter a node before the client save it
 * @param object $node Object representation of a node
 */
function channels_client_channels_client_alter_node($node) {
  
  if(isset($node->is_new)) unset($node->is_new);
  if(isset($node->process_api_language)) unset($node->process_api_language);
  
  $formats = filter_formats();
  
  $body_fields = array();
  foreach ($node as $key => $value) {
    if (preg_match('/^body:/', $key)) {
     $body_fields[$key] = $value;
     unset($node->{$key}); 
    }
  }

  foreach ($body_fields as $key => $value) {
    $ids = preg_split('/\:/', $key);
    if (isset($ids[1]) ) {
      switch ($ids[1]) {
        
        case 'value' :  
          $node->body = $value;
          break;
          
        case 'summary' :   
          $node->teaser = $value;  
          break;
        case 'format' :  
          if ($value == 'plain_text') $node->format = 0;
          elseif ($value == 'filtered_html') $node->format = 1;
          elseif ($value == 'full_html') $node->format = 2;
          break;
      }
    } 
  }
  return $node;
}